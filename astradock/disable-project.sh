#!/bin/bash -e
set -e

config_path="/etc/apache2/sites-available"

configs=$(find $config_path -name "*.conf" ! -name "000-default.conf" -printf '%f\n')

if [[ $configs == "" ]]; then
	echo "Включенные конфигурации отсутствуют. Завершение работы скрипта."
	exit 2
fi

configs=($configs)
prefix="_disabled"

for i in "${!configs[@]}"; do
	echo  "$(($i + 1))) ${configs[$i]}"
done

read -p "Укажите индекс проекта, который вы хотите отключить: " selected_index

index_exist=0
selected_index=$(($selected_index - 1))

for i in "${!configs[@]}"; do
	if [[ "$i" == "$(($selected_index))" ]]; then
		index_exist=1
	fi
done


if [[ "$index_exist" == 0 ]]; then
	echo "Введённый вами индекс некорректен."
	exit 22
fi

cd $config_path
mv ${configs[$selected_index]} ${configs[$selected_index]}${prefix}


echo "Виртуальный хост ${configs[$selected_index]} отключен."
